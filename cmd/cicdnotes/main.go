package main

import (
	"html/template"
	"log"
	"net/http"

	"notes/pkg/handlers"
	"notes/pkg/note"

	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

// run only from terminal in the same directory as "static"
func main() {
	templates := template.Must(template.ParseGlob("/home/ubuntu/templates/index.html"))

	zapLogger, _ := zap.NewProduction()
	defer func() {
		if err := zapLogger.Sync(); err != nil {
			log.Fatal(err)
		}
	}()

	logger := zapLogger.Sugar()

	noteRepo := note.NewNoteRepo()
	handler := &handlers.NoteHandler{
		Tmpl:     templates,
		Logger:   logger,
		NoteRepo: noteRepo,
	}

	r := mux.NewRouter()
	r.HandleFunc("/", handler.Index).Methods("GET")
	r.HandleFunc("/note", handler.GetAllNotesByOrder).Methods("GET")
	r.HandleFunc("/note/{id}", handler.GetNoteByID).Methods("GET")
	r.HandleFunc("/note", handler.AddNote).Methods("POST")
	r.HandleFunc("/note/{id}", handler.EditNote).Methods("PUT")
	r.HandleFunc("/note/{id}", handler.DeleteNote).Methods("DELETE")

	r.PathPrefix("/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("/home/ubuntu/templates/static"))))

	addr := ":80"
	logger.Infow("starting server",
		"type", "START",
		"addr", addr,
	)

	err := http.ListenAndServe(addr, r)
	if err != nil {
		log.Printf("ListenAndServe error")
		return
	}
}
