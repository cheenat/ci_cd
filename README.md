cheena's msu-golang-hw8-CI_CD 

Get all notes:
```
curl 87.239.110.21/note
curl -X GET "http://87.239.110.21/note?order_by=Text" (with order)
```

Get note by id:
```
curl 87.239.110.21/note/0
```
Add note:
```
curl -X POST -d '{"Text": "new note"}' 87.239.110.21/note
```
Edit note:
```
curl -X PUT -d '{"Text": "edited text"}' http://87.239.110.21/note/3
```
Delete note:
```
curl -X DELETE 87.239.110.21/note/0
```