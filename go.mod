module notes

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/schema v1.2.0
	github.com/labstack/echo/v4 v4.1.17
	github.com/stretchr/testify v1.4.0
	go.uber.org/zap v1.16.0
	golang.org/x/mod v0.0.0-20190513183733-4bf6d317e70e
)
