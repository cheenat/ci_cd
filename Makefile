.PHONY: build
build:
	go build -o bin/app ./cmd/cicdnotes/main.go

.PHONY: lint
lint:
	golangci-lint run -v

.PHONY: test
test:
	go test -v -coverpkg=./... -coverprofile=profile.cov ./...
	go tool cover -func profile.cov