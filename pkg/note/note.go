package note

type Note struct {
	ID        uint32
	Text      string
	CreatedAt string
	UpdatedAt string
}
