package note

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestRepo(t *testing.T) {
	tm := time.Now()
	tmRFC := tm.Format(time.RFC3339)
	rightAnswer := Note{
		ID:        0,
		Text:      "Zebra is a horse with stripes",
		CreatedAt: tmRFC,
		UpdatedAt: tmRFC,
	}
	repo := NewNoteRepo()
	item, err := repo.GetByID(0)
	if err != nil {
		t.Errorf("unexpected err: %s", err)
		return
	}

	assert.Equal(t, rightAnswer, item)

	_, err = repo.GetByID(234)
	if err == nil {
		t.Errorf("expected error, got nil")
		return
	}

	thirdNote := Note{
		ID:        2,
		Text:      "And I would like to use standart library",
		CreatedAt: tmRFC,
		UpdatedAt: tmRFC,
	}

	secondNote := Note{
		ID:        1,
		Text:      "Best regards to professor",
		CreatedAt: tmRFC,
		UpdatedAt: tmRFC,
	}

	firstNote := Note{
		ID:        0,
		Text:      "Zebra is a horse with stripes",
		CreatedAt: tmRFC,
		UpdatedAt: tmRFC,
	}

	rightRes := []Note{
		thirdNote,
		secondNote,
		firstNote,
	}
	_ = rightRes

	res, err := repo.GetAll("Text")
	if err != nil {
		t.Errorf("unexpected err: %s", err)
		return
	}

	assert.Equal(t, rightRes, res)

	rightRes = []Note{
		firstNote,
		secondNote,
		thirdNote,
	}

	res, err = repo.GetAll("")
	if err != nil {
		t.Errorf("unexpected err: %s", err)
		return
	}

	assert.Equal(t, rightRes, res)

	lastID := uint32(3)
	item = Note{Text: "new note"}

	r, err := repo.Add(item)
	if err != nil {
		t.Errorf("unexpected err: %s", err)
		return
	}

	assert.Equal(t, lastID, r)

	deleteID := uint32(2)

	deleteRes, err := repo.Delete(deleteID)
	if err != nil {
		t.Errorf("unexpected err: %s", err)
		return
	}

	assert.Equal(t, true, deleteRes)

	deleteID = uint32(70)
	_, err = repo.Delete(deleteID)
	if err == nil {
		t.Errorf("expected error, got nil")
		return
	}
}

func TextUpdate(t *testing.T) {
	repo := NewNoteRepo()

	item := Note{ID: 70, Text: "edited text"}

	_, err := repo.Update(item)
	if err == nil {
		t.Errorf("expected error, got nil")
		return
	}

	item = Note{ID: 1, Text: "edited text"}

	updateRes, err := repo.Update(item)
	if err != nil {
		t.Errorf("unexpected err: %s", err)
		return
	}

	assert.Equal(t, true, updateRes)
}
