package note

import (
	"errors"
	"reflect"
	"sort"
	"sync"
	"time"
)

type Repo struct {
	LastID  uint32
	DataMap map[uint32]Note
	Mutex   *sync.RWMutex
}

func NewNoteRepo() *Repo {
	t := time.Now()
	timeRFC := t.Format(time.RFC3339)

	return &Repo{
		Mutex:  &sync.RWMutex{},
		LastID: 2,
		DataMap: map[uint32]Note{
			0: Note{
				ID:        0,
				Text:      "Zebra is a horse with stripes",
				CreatedAt: timeRFC,
				UpdatedAt: timeRFC,
			},
			1: Note{
				ID:        1,
				Text:      "Best regards to professor",
				CreatedAt: timeRFC,
				UpdatedAt: timeRFC,
			},
			2: Note{
				ID:        2,
				Text:      "And I would like to use standart library",
				CreatedAt: timeRFC,
				UpdatedAt: timeRFC,
			},
		}}
}

var (
	ErrNotFound = errors.New("note not found")
)

func (repo *Repo) GetAll(orderBy string) ([]Note, error) { // add case notation + UpdatedAt
	repo.Mutex.Lock()
	defer repo.Mutex.Unlock()

	keys := make([]Note, 0, len(repo.DataMap))
	for _, k := range repo.DataMap {
		keys = append(keys, k)
	}

	if orderBy == "" {
		orderBy = "ID"
	}

	sort.Slice(keys, func(i, j int) bool {
		rPrev := reflect.ValueOf(keys[i])
		prev := reflect.Indirect(rPrev).FieldByName(orderBy)
		rNext := reflect.ValueOf(keys[j])
		next := reflect.Indirect(rNext).FieldByName(orderBy)
		if prev.Type().Kind() == reflect.String {
			return prev.String() < next.String()
		}
		return prev.Uint() < next.Uint()
	})

	return keys, nil
}

func (repo *Repo) GetByID(id uint32) (Note, error) {
	repo.Mutex.Lock()
	defer repo.Mutex.Unlock()

	if item, ok := repo.DataMap[id]; ok {
		return item, nil
	}

	return Note{}, ErrNotFound
}

func (repo *Repo) Add(item Note) (uint32, error) {
	repo.Mutex.Lock()
	defer repo.Mutex.Unlock()

	repo.LastID++
	t := time.Now()
	item.CreatedAt = t.Format(time.RFC3339)
	item.UpdatedAt = item.CreatedAt
	item.ID = repo.LastID
	repo.DataMap[item.ID] = item
	return repo.LastID, nil
}

func (repo *Repo) Update(newItem Note) (bool, error) {
	repo.Mutex.Lock()
	defer repo.Mutex.Unlock()

	if _, ok := repo.DataMap[newItem.ID]; ok {
		t := time.Now()
		newItem.UpdatedAt = t.Format(time.RFC3339)
		repo.DataMap[newItem.ID] = newItem
		return true, nil
	}

	return false, ErrNotFound
}

func (repo *Repo) Delete(id uint32) (bool, error) {
	repo.Mutex.Lock()
	defer repo.Mutex.Unlock()

	if _, ok := repo.DataMap[id]; ok {
		delete(repo.DataMap, id)
		return true, nil
	}

	return false, ErrNotFound
}
