package handlers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"

	"notes/pkg/note"

	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

type NoteHandler struct {
	Tmpl     *template.Template
	NoteRepo *note.Repo
	Logger   *zap.SugaredLogger
}

func (h *NoteHandler) Index(w http.ResponseWriter, r *http.Request) {
	err := h.Tmpl.ExecuteTemplate(w, "index.html", nil)
	if err != nil {
		jsonError(w, http.StatusInternalServerError, `Template errror`)
		return
	}
}

func (h *NoteHandler) GetNoteByID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		jsonError(w, http.StatusBadGateway, "Bad ID")
		return
	}

	item, err := h.NoteRepo.GetByID(uint32(id))
	if err != nil {
		jsonError(w, http.StatusInternalServerError, "DB error")
		return
	}
	if (item == note.Note{}) {
		jsonError(w, http.StatusNotFound, "no item with ID:"+vars["id"])
		return
	}

	resp, err := json.Marshal(item)
	if err != nil {
		fmt.Println(err)
		return
	}
	_, err = w.Write(resp)
	if err != nil {
		log.Fatal(err)
		return
	}
}

func (h *NoteHandler) AddNote(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)

	var item note.Note
	err := decoder.Decode(&item)

	if err != nil {
		jsonError(w, http.StatusInternalServerError, "Decode error")
		return
	}

	lastID, err := h.NoteRepo.Add(item)
	if err != nil {
		jsonError(w, http.StatusInternalServerError, "DB error")
		return
	}

	respJSON, err := json.Marshal(map[string]uint32{
		"added note with ID:": lastID,
	})
	if err != nil {
		fmt.Println(err)
		return
	}
	_, err = w.Write(respJSON)
	if err != nil {
		log.Fatal(err)
		return
	}
}

func (h *NoteHandler) EditNote(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		jsonError(w, http.StatusBadRequest, "Bad ID")
		return
	}

	decoder := json.NewDecoder(r.Body)

	var item note.Note
	err = decoder.Decode(&item)

	if err != nil {
		jsonError(w, http.StatusInternalServerError, "Decode error")
		return
	}

	item.ID = uint32(id)

	ok, err := h.NoteRepo.Update(item)
	if err != nil {
		jsonError(w, http.StatusInternalServerError, "DB error")
		return
	}

	respJSON, err := json.Marshal(map[string]bool{
		"success editing": ok,
	})
	if err != nil {
		fmt.Println(err)
		return
	}
	_, err = w.Write(respJSON)
	if err != nil {
		log.Fatal(err)
		return
	}
}

func (h *NoteHandler) DeleteNote(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		jsonError(w, http.StatusBadGateway, "bad id")
		return
	}

	ok, err := h.NoteRepo.Delete(uint32(id))
	if err != nil {
		jsonError(w, http.StatusInternalServerError, err.Error())
		return
	}

	w.Header().Set("Content-type", "application/json")
	respJSON, err := json.Marshal(map[string]bool{
		"success": ok,
	})
	if err != nil {
		fmt.Println(err)
		return
	}
	_, err = w.Write(respJSON)
	if err != nil {
		log.Fatal(err)
		return
	}
}

func (h *NoteHandler) GetAllNotesByOrder(w http.ResponseWriter, r *http.Request) {
	orderBy := r.FormValue("order_by")

	elems, err := h.NoteRepo.GetAll(orderBy)
	if err != nil {
		jsonError(w, http.StatusInternalServerError, err.Error())
		return
	}

	resp, err := json.Marshal(elems)
	if err != nil {
		fmt.Println(err)
		return
	}
	_, err = w.Write(resp)
	if err != nil {
		log.Fatal(err)
		return
	}
}

func jsonError(w http.ResponseWriter, statusCode int, msg string) {
	resp, _ := json.Marshal(map[string]interface{}{
		"message": msg,
	})
	w.WriteHeader(statusCode)
	_, err := w.Write(resp)
	if err != nil {
		log.Fatal(err)
	}
}
