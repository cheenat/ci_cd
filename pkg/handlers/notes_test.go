package handlers

import (
	"bytes"
	"encoding/json"
	"html/template"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"notes/pkg/note"
	"testing"
	"time"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

func TestHandler(t *testing.T) {
	noteRepo := note.NewNoteRepo()
	templates := template.Must(template.ParseGlob("../../templates/index.html"))

	handlers := &NoteHandler{
		NoteRepo: noteRepo,
		Tmpl:     templates,
	}

	vars := map[string]string{
		"id": "0",
	}
	tm := time.Now()
	rightAnswer := &note.Note{
		ID:        0,
		Text:      "Zebra is a horse with stripes",
		CreatedAt: tm.Format(time.RFC3339),
		UpdatedAt: tm.Format(time.RFC3339),
	}

	req := httptest.NewRequest("GET", "/note/0", nil)
	req = mux.SetURLVars(req, vars)
	w := httptest.NewRecorder()

	handlers.GetNoteByID(w, req)
	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	jsonRightResp, _ := json.Marshal(rightAnswer)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, jsonRightResp, body)

	vars = map[string]string{
		"id": "first",
	}
	req = httptest.NewRequest("GET", "/note/first", nil)
	req = mux.SetURLVars(req, vars)
	w = httptest.NewRecorder()

	handlers.GetNoteByID(w, req)
	resp = w.Result()
	defer resp.Body.Close()

	if resp.StatusCode != 502 {
		t.Errorf("expected resp status 502, got %d", resp.StatusCode)
		return
	}

	vars = map[string]string{
		"id": "3",
	}
	req = httptest.NewRequest("GET", "/note/3", nil)
	req = mux.SetURLVars(req, vars)
	w = httptest.NewRecorder()

	handlers.GetNoteByID(w, req)
	resp = w.Result()
	defer resp.Body.Close()

	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}

	vars = map[string]string{
		"id": "0",
	}
	reqPayload, _ := json.Marshal(map[string]interface{}{
		"Text": "new text",
	})
	b := bytes.NewBuffer(reqPayload)

	req = httptest.NewRequest("PUT", "/note/0", b)
	req = mux.SetURLVars(req, vars)
	w = httptest.NewRecorder()

	handlers.EditNote(w, req)
	resp = w.Result()
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		t.Errorf("expected resp status 200, got %d", resp.StatusCode)
		return
	}
}

func TestIndex(t *testing.T) {
	noteRepo := note.NewNoteRepo()
	templates := template.Must(template.ParseGlob("../../templates/index.html"))

	handlers := &NoteHandler{
		NoteRepo: noteRepo,
		Tmpl:     templates,
	}

	req := httptest.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	handlers.Index(w, req)
	resp := w.Result()
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		t.Errorf("expected resp status 200, got %d", resp.StatusCode)
		return
	}
}

func TestAddNote(t *testing.T) {
	noteRepo := note.NewNoteRepo()
	templates := template.Must(template.ParseGlob("../../templates/index.html"))

	handlers := &NoteHandler{
		NoteRepo: noteRepo,
		Tmpl:     templates,
	}

	reqPayload, _ := json.Marshal(map[string]interface{}{
		"Text": "new note",
	})

	b := bytes.NewBuffer(reqPayload)

	req := httptest.NewRequest("POST", "/note", b)
	w := httptest.NewRecorder()

	handlers.AddNote(w, req)
	resp := w.Result()
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		t.Errorf("expected resp status 200, got %d", resp.StatusCode)
		return
	}
}

func TestDeleteNote(t *testing.T) {
	noteRepo := note.NewNoteRepo()
	templates := template.Must(template.ParseGlob("../../templates/index.html"))

	handlers := &NoteHandler{
		NoteRepo: noteRepo,
		Tmpl:     templates,
	}

	vars := map[string]string{
		"id": "0",
	}

	req := httptest.NewRequest("DELETE", "/note/0", nil)
	req = mux.SetURLVars(req, vars)
	w := httptest.NewRecorder()

	handlers.DeleteNote(w, req)
	resp := w.Result()
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		t.Errorf("expected resp status 200, got %d", resp.StatusCode)
		return
	}

	req = httptest.NewRequest("DELETE", "/note/0", nil)
	req = mux.SetURLVars(req, vars)
	w = httptest.NewRecorder()

	handlers.DeleteNote(w, req)
	resp = w.Result()
	defer resp.Body.Close()

	if resp.StatusCode != 500 {
		t.Errorf("expected resp status 500, got %d", resp.StatusCode)
		return
	}
}

func TestGetAllNotesByOrder(t *testing.T) {
	noteRepo := note.NewNoteRepo()
	templates := template.Must(template.ParseGlob("../../templates/index.html"))

	handlers := &NoteHandler{
		NoteRepo: noteRepo,
		Tmpl:     templates,
	}

	req := httptest.NewRequest("GET", "/note/?order_by=Text", nil)
	w := httptest.NewRecorder()

	handlers.GetAllNotesByOrder(w, req)
	resp := w.Result()
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		t.Errorf("expected resp status 200, got %d", resp.StatusCode)
		return
	}
}
